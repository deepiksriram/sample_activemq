package it.janczewski.examples.utils;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Sender {
    private final Logger logger = LoggerFactory.getLogger(Sender.class);
    private final SecureRandom random = new SecureRandom();
    public static void main(String[] args) throws Exception {
        Sender sender = new Sender();
        sender.createTask();
    }

    public void createTask() throws  Exception{
        String taskName = generateTaskName();
        Runnable sendTask = () -> {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://127.0.0.1:61616/");
            try {
                Connection connection = connectionFactory.createConnection();
                connection.start();
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);//
                Destination destination = session.createQueue("CM_Test");
                MessageProducer producer = session.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);//
                String text = "Hello from: " + taskName + " : " + this.hashCode();
                TextMessage message = session.createTextMessage(text);
                logger.info("Sent message hash code: "+ message.hashCode() + " : " + taskName);
                producer.send(message);


            } catch (Exception e) {
                logger.error("Sender createTask method error", e);
            }


        };
        new Thread(sendTask).start();
    }

    private String generateTaskName() {
        return new BigInteger(20, random).toString(16);
    }


        private final String connectionUri = "tcp://localhost:61616";
        private ActiveMQConnectionFactory connectionFactory;
        private Connection connection;
        private Session session;
        private Destination destination;
        private static final int NUM_REQUESTS = 10;
        private final CountDownLatch done = new CountDownLatch(NUM_REQUESTS);

        public void before() throws Exception {
            connectionFactory = new
                    ActiveMQConnectionFactory(connectionUri);
            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(
                    false, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue("REQUEST.QUEUE");
        }

        public void run() throws Exception {
            TemporaryQueue responseQ = session.createTemporaryQueue();
            MessageProducer requester =
                    session.createProducer(destination);
            MessageConsumer responseListener =
                    session.createConsumer(responseQ);
            responseListener.setMessageListener((MessageListener) this);

            for (int i = 0; i < NUM_REQUESTS; i++) {
                TextMessage request =
                        session.createTextMessage("Job Request");
                request.setJMSReplyTo(responseQ);
                request.setJMSCorrelationID("request: " + i);
                requester.send(request);
            }
            if (done.await(10, TimeUnit.MINUTES)) {
                System.out.println("Woohoo! Work's all done!");
            }
            else {
                System.out.println("Doh!! Work didn't get done.");
            }
        }

        public void onMessage(Message message) {
            try {
                String jmsCorrelation = message.getJMSCorrelationID();
                if (!jmsCorrelation.startsWith("request")) {
                    System.out.println("Received an unexpected response: " + jmsCorrelation);
                }
                TextMessage txtResponse = (TextMessage) message;
                System.out.println(txtResponse.getText());
                done.countDown();
            }
            catch (Exception ex) {
            }
        }

}
